provider "aws" {
#    access_key = "" obtained from ~/.aws/credentials
#    secret_key = "" obtained from ~/.aws/credentials
    region = "us-east-1"
}

resource "aws_instance" "terra_ex" {
    ami = "ami-2757f631"
    instance_type = "t2.micro"
}